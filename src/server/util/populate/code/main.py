import os
import sys
import json
from datetime import date, datetime
from dotenv import load_dotenv

from parsing.db.connection import get_db
from parsing.entities.parser_info import insert_parser_info
from parsing.util import write_to_log, BASE_DIR
from parsing.parser.xml.xml_parser import XMLParser

def make_log_path():
    if not os.path.exists(f'{BASE_DIR}/util/populate/logs/parser'):
        os.makedirs(f'{BASE_DIR}/util/populate/logs/parser')

    return f"{BASE_DIR}/util/populate/logs/parser/{date.today().strftime('%d-%m-%Y')}_parse_errors.log"

def should_use_verbose_mode():
    verbose = False
    if len(sys.argv) > 1 and sys.argv[1] == "-v":
        verbose = True

    return verbose

def get_institute():
    load_dotenv()
    INSTITUTE = os.environ.get('INSTITUTE', '').lower()
    if INSTITUTE == "":
        raise Exception("Instituto não foi especificado. Isso pode ser feito atribuindo um valor à variável $INSTITUTE")

    return INSTITUTE

def get_professors_by_dep(institute_initials):
    with open(f'{BASE_DIR}/util/{institute_initials}/docentes.json', 'r') as f:
        json_data = f.read()

    return json.loads(json_data)

def main():
    log_path = make_log_path()

    write_to_log(log_path, f"Parser inicializado.\n")

    INSTITUTE = get_institute()
    lattes_path = f'{BASE_DIR}/util/{INSTITUTE}/lattes'
    conn, cur = get_db()
    parser = XMLParser(should_use_verbose_mode(),
                       get_professors_by_dep(INSTITUTE),
                       conn,
                       cur
                      )

    print("Iniciando verificação de estrutura dos arquivos...")
    print()

    # Check for syntactic errors
    for root, _, files in os.walk(lattes_path):
        for file in files:
            filepath = os.path.join(root, file)
            if parser.has_correct_extension(filepath) and not parser.is_well_formed(filepath):
                print(f"Erro encontrado no arquivo '{filepath}'. Abortando execução")
                print()
                return

    print("Nenhum erro encontrado")
    print()

    print("Inserindo informações dos docentes...")
    print()

    # Insert professors' data
    for root, _, files in os.walk(lattes_path):
        for file in files:
            filepath = os.path.join(root, file)
            if parser.has_correct_extension(filepath):
                parser.insert_professor_information(filepath)

    write_to_log(log_path, f"Todos os professores inseridos no banco.\n")

    print("Finalizado")
    print()

    print("Inserindo informações dos currículos lattes...")
    print()

    # Insert lattes data
    for root, _, files in os.walk(lattes_path):
        for file in files:
            filepath = os.path.join(root, file)
            if parser.has_correct_extension(filepath):
                parser.insert_lattes_information(filepath)

    write_to_log(log_path, f"Todos os dados inseridos no banco.\nParser encerrado.\n")

    print("Finalizado")
    print()

    insert_parser_info({ "date": datetime.now() }, conn, cur)

if __name__ == "__main__":
    main()
