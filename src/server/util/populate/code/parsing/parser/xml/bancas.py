from html import unescape
from bs4 import BeautifulSoup

from ... import util


def parse_bancas(root, num_identificador):
    bancas_array = []

    for bancas in root.findall("DADOS-COMPLEMENTARES/PARTICIPACAO-EM-BANCA-TRABALHOS-CONCLUSAO"):
        for banca in bancas:
            tipo = None
            dados_banca = None
            detalhe_banca = None

            if banca.tag == "OUTRAS-PARTICIPACOES-EM-BANCA":
                dados_banca = banca.find("DADOS-BASICOS-DE-OUTRAS-PARTICIPACOES-EM-BANCA")
                detalhe_banca = banca.find("DETALHAMENTO-DE-OUTRAS-PARTICIPACOES-EM-BANCA")
                tipo = dados_banca.attrib['NATUREZA']
            else:
                tipo = banca.tag[25:]
                dados_banca = banca.find("DADOS-BASICOS-DA-PARTICIPACAO-EM-BANCA-DE-{}".format(tipo))
                detalhe_banca = banca.find("DETALHAMENTO-DA-PARTICIPACAO-EM-BANCA-DE-{}".format(tipo))

            if dados_banca is None:
                print("Exception in parse_bancas: {} with {}".format(num_identificador, banca.tag))

            bancas_dict = {}
            bancas_dict['id'] = ""
            bancas_dict['pessoa_id'] = num_identificador
            bancas_dict['aluno'] = detalhe_banca.attrib['NOME-DO-CANDIDATO']
            bancas_dict['ano'] = dados_banca.attrib['ANO']            
            bancas_dict['titulo'] = BeautifulSoup(unescape(dados_banca.attrib['TITULO']), 'lxml').text
            bancas_dict['pais'] = dados_banca.attrib['PAIS']
            util.check_country_exists(bancas_dict['pais'], bancas_dict['titulo'],
                [bancas_dict['aluno']])

            bancas_dict['instituicao'] = detalhe_banca.attrib['NOME-INSTITUICAO']
            bancas_dict['tipo_pesquisa'] = tipo.replace("-", "_").lower().title()

            try:
                bancas_dict['idioma'] = dados_banca.attrib['IDIOMA']
            except:
                bancas_dict['idioma'] = ""
            try:
                bancas_dict['doi'] = dados_banca.attrib['DOI']
            except:
                bancas_dict['doi'] = ""
            try:
                bancas_dict['curso'] = detalhe_banca.attrib['NOME-CURSO'].title()
            except:
                bancas_dict['curso'] = ""

            bancas_array.append(bancas_dict)

    for bancas in root.findall("DADOS-COMPLEMENTARES/PARTICIPACAO-EM-BANCA-JULGADORA"):
        for banca in bancas:

            tipo = ""
            dados_banca = None
            detalhe_banca = None

            if banca.tag == "OUTRAS-BANCAS-JULGADORAS":
                dados_banca = banca.find("DADOS-BASICOS-DE-OUTRAS-BANCAS-JULGADORAS")
                detalhe_banca = banca.find("DETALHAMENTO-DE-OUTRAS-BANCAS-JULGADORAS")
                tipo = dados_banca.attrib["NATUREZA"]
            else:
                tipo = banca.tag[21:]
                dados_banca = banca.find("DADOS-BASICOS-DA-BANCA-JULGADORA-PARA-{}".format(tipo))
                detalhe_banca = banca.find("DETALHAMENTO-DA-BANCA-JULGADORA-PARA-{}".format(tipo))

            if dados_banca is None:
                print("Exception in parse_bancas: {} with {}".format(num_identificador, banca.tag))

            bancas_dict = {}
            bancas_dict['id'] = ""
            bancas_dict['pessoa_id'] = num_identificador
            bancas_dict['aluno'] = ""
            bancas_dict['ano'] = dados_banca.attrib['ANO']
            bancas_dict['titulo'] = BeautifulSoup(unescape(dados_banca.attrib['TITULO']), 'lxml').text
            bancas_dict['pais'] = dados_banca.attrib['PAIS']
            util.check_country_exists(bancas_dict['pais'], bancas_dict['titulo'],
                [bancas_dict['aluno']])

            bancas_dict['instituicao'] = detalhe_banca.attrib['NOME-INSTITUICAO']
            bancas_dict['tipo_pesquisa'] = tipo.replace("-", "_").lower().title()

            try:
                bancas_dict['idioma'] = dados_banca.attrib['IDIOMA']
            except:
                bancas_dict['idioma'] = ""
            try:
                bancas_dict['doi'] = dados_banca.attrib['DOI']
            except:
                bancas_dict['doi'] = ""
            try:
                bancas_dict['curso'] = detalhe_banca.attrib['NOME-CURSO'].title()
            except:
                bancas_dict['curso'] = ""

            bancas_array.append(bancas_dict)

    return bancas_array

