from html import unescape
from bs4 import BeautifulSoup


def parse_participacao(root, num_identificador):
    participacoes_array = []

    for participacoes in root.findall("DADOS-COMPLEMENTARES/PARTICIPACAO-EM-EVENTOS-CONGRESSOS"):
        for participacao in participacoes:
            tipo = participacao.tag
            artigo = "O"
            dados_participacao = participacao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_participacao is None:
                artigo = "A"
                dados_participacao = participacao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_participacao is None:
                artigo = "E"
                dados_participacao = participacao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_participacao is None:
                print("Exception in parse_participacao: {} with {}".format(num_identificador, participacao.tag))


            detalhamento_participacao = participacao.find("DETALHAMENTO-D{}-{}".format(artigo, tipo))

            if detalhamento_participacao is None:
                print("Exception in parse_participacao: {} don't have DETALHAMENTO".format(num_identificador))
                continue

            participacoes_dict = {}
            participacoes_dict['id'] = ""
            participacoes_dict['tipo'] = participacao.tag.replace("-", "_").lower().title()
            participacoes_dict['natureza'] = dados_participacao.attrib['NATUREZA']
            participacoes_dict['titulo'] = BeautifulSoup(unescape(dados_participacao.attrib['TITULO']), 'lxml').text
            participacoes_dict['pais'] = dados_participacao.attrib['PAIS']
            participacoes_dict['ano'] = dados_participacao.attrib['ANO']
            participacoes_dict['tipo'] = dados_participacao.attrib['TIPO-PARTICIPACAO'].title()
            participacoes_dict['forma'] = dados_participacao.attrib['FORMA-PARTICIPACAO']

            try:
                participacoes_dict['nome_ev'] = detalhamento_participacao.attrib['NOME-DO-EVENTO']
            except:
                participacoes_dict['nome_ev'] = ""
            try:
                participacoes_dict['idioma'] = dados_participacao.attrib['IDIOMA']
            except:
                participacoes_dict['idioma'] = ""
            try:
                participacoes_dict['meio'] = dados_participacao.attrib['MEIO-DE-DIVULGACAO']
            except:
                participacoes_dict['meio'] = ""
            try:
                participacoes_dict['flag'] = dados_participacao.attrib['FLAG-RELEVANCIA']
            except:
                participacoes_dict['flag'] = ""
            try:                
                participacoes_dict['doi'] = dados_participacao.attrib['DOI']
            except:
                participacoes_dict['doi'] = ""
            try:                
                participacoes_dict['nome_inst'] = detalhamento_participacao.attrib['NOME-INSTITUICAO']
            except:
                participacoes_dict['nome_inst'] = ""
            autores = []
            for autor in participacao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            participacoes_dict['autores'] = autores

            participacoes_array.append(participacoes_dict)

    return participacoes_array
