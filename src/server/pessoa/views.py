from django.db.models import Count
from django.http import JsonResponse
from models.models import Pessoa
from util.populate.code.parsing.util import normalize_string
import util.fau.route_operations as route_operations
from . import services
import json
import datetime
import re
import requests


now = datetime.datetime.now()
actual_year = str(now.year)


def index(request):
    """Index for pessoa route.

    Parameters: a GET request with 'idLattes' parameter.

    Returns:
    JsonResponse with status code and all the information of the person
    with the given id.
    """

    id_lattes = request.GET.get('id', '')

    if not id_lattes.isdigit():
        return route_operations.error('Id inválido')

    id_lattesInt = int(id_lattes)
    query_set = Pessoa.objects.filter(id_lattes=id_lattesInt)
    if not query_set:
        return route_operations.error('Id inválido')
    result = {
        "id_lattes": id_lattes,
        "nome_completo": query_set[0].nome_completo,
        "nome_citacao": query_set[0].nome_citacao,
        "nacionalidade": query_set[0].nacionalidade,
        "contato": query_set[0].contato,
        "departamento": query_set[0].departamento,
        "cidade": query_set[0].cidade,
        "estado": query_set[0].estado,
        "resumo": query_set[0].resumo,
        "producoes_bibliograficas": [],
        "producoes_artisticas": [],
        "producoes_tecnicas": [],
        "bancas": [],
        "orientacoes": [],
        "premios_titulos": [],
        "participacoes": []
    }
    for prod_bib in query_set[0].producoes_bibliograficas.all():
        autores = []
        for autor in prod_bib.nomes_autores.split(","):
            autores.append(re.sub(r'[\"\{\}]', '', autor))
        result["producoes_bibliograficas"].append({
            "id": prod_bib.id,
            "tipo": prod_bib.tipo.replace('_', ' '),
            "titulo": prod_bib.titulo.replace('_', ' '),
            "ano": prod_bib.ano,
            "autores": autores,
            "publicacao": prod_bib.publicacao,
            "editora": prod_bib.editora,
            "edicao": prod_bib.edicao,
            "volume": prod_bib.volume,
            "natureza": prod_bib.natureza,
            "pais": prod_bib.pais,
            "idioma": prod_bib.idioma,
            "meio": prod_bib.meio,
            "flag": prod_bib.flag,
            "doi": prod_bib.doi,
            "isbn": prod_bib.isbn
        })
    for prod_art in query_set[0].producoes_artisticas.all():
        autores = []
        for autor in prod_art.nomes_autores.split(","):
            autores.append(re.sub(r'[\"\{\}]', '', autor))
        result["producoes_artisticas"].append({
            "id": prod_art.id,
            "tipo": prod_art.tipo.replace('_', ' '),
            "titulo": prod_art.titulo.replace('_', ' '),
            "ano": prod_art.ano,
            "autores": autores,
            "natureza": prod_art.natureza,
            "pais": prod_art.pais,
            "meio": prod_art.meio,
            "flag": prod_art.flag,
            "doi": prod_art.doi,
            "idioma": prod_art.idioma,
            "premiacao": prod_art.premiacao,
            "exposicao": prod_art.exposicao
        })
    for prod_tec in query_set[0].producoes_tecnicas.all():
        autores = []
        for autor in prod_tec.nomes_autores.split(","):
            autores.append(re.sub(r'[\"\{\}]', '', autor))
        result["producoes_tecnicas"].append({
            "id": prod_tec.id,
            "tipo": prod_tec.tipo.replace('_', ' '),
            "titulo": prod_tec.titulo.replace('_', ' '),
            "ano": prod_tec.ano,
            "autores": autores,
            "natureza": prod_tec.natureza,
            "pais": prod_tec.pais,
            "idioma": prod_tec.idioma,
            "meio": prod_tec.meio,
            "flag": prod_tec.flag,
            "finalidade": prod_tec.finalidade,
            "pags": prod_tec.pags
        })
    for banca in query_set[0].aparicoes_bancas.all():
        result["bancas"].append({
            "id": banca.id,
            "aluno": banca.aluno,
            "ano": banca.ano,
            "titulo": banca.titulo.replace('_', ' '),
            "pais": banca.pais,
            "instituicao": banca.instituicao,
            "tipo": banca.tipo_pesquisa.replace('_', ' '),
            "idioma": banca.idioma,
            "doi": banca.doi,
            "curso": banca.curso
        })
    for ori in query_set[0].orientacao_set.all():
        result["orientacoes"].append({
            "id": ori.id,
            "aluno": ori.aluno,
            "ano": ori.ano,
            "titulo": ori.titulo.replace('_', ' '),
            "pais": ori.pais,
            "instituicao": ori.instituicao,
            "agencia_fomento": ori.agencia_fomento,
            "tipo_orientacao": ori.tipo_orientacao.replace('_', ' '),
            "tipo_pesquisa": ori.tipo_pesquisa.replace('_', ' ').title(),
            "natureza": ori.natureza,
            "idioma": ori.idioma,
            "flag": ori.flag,
            "doi": ori.doi,
            "curso": ori.curso
        })
    for titulo in query_set[0].premiostitulos_set.all():
        result["premios_titulos"].append({
            "id": titulo.id,
            "nome": titulo.nome.replace('_', ' '),
            "ano": titulo.ano,
            "entidade": titulo.entidade
        })
    for part in query_set[0].participacoes.all():
        result["participacoes"].append({
            "id": part.id,
            "nome_ev": part.nome_ev.replace('_', ' '),
            "tipo": part.tipo.replace('_', ' '),
            "ano": part.ano,
            "natureza": part.natureza,
            "pais": part.pais,
            "idioma": part.idioma,
            "meio": part.meio,
            "flag": part.flag,
            "doi": part.doi,
            "nome_inst": part.nome_inst
        })

    return JsonResponse(result)

def names(request):
    """Names for pessoa route.

    Parameters: a GET request.

    Returns:
    JsonResponse with status code and all names, idLattes
    and department of people on the database.
    """
    return JsonResponse({"names": services.get_names()})


def countByDep(request):
    """countByDep for pessoa route.

    Parameters: a GET request with 'departmento' parameter.

    Returns:
    JsonResponse with status code and a count of people
    in the given department.
    """

    department = request.GET.get('departamento', '')
    if department not in ['AUH', 'AUP', 'AUT']:
        return route_operations.error('Departamento inexistente')

    query_set = Pessoa.objects.filter(departamento=department).count()

    return JsonResponse(query_set)


def keywords(request):
    """Keywords for pessoa route.

    Parameters: a GET request with 'idLattes' and
    optional 'limit' parameter.

    Returns:
    JsonResponse with status code and a list of words with its frequencies.
    """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return route_operations.error('Limite inválido.')

    if not limit > 0:
        return route_operations.error('Limite inválido.')

    idLattes = request.GET.get('id', '')
    keywords = Pessoa.keywords(idLattes, limit)
    if keywords == 'erro':
        return route_operations.error('Id inválido.')
    result = []

    for keyword in keywords:
        result.append({keyword[0]: keyword[1]})

    return JsonResponse({"keywords": result})


def map(request):
    """Map for producao_artistica route.

    Parameters: a GET request with 'idLattes' and
    optional 'limit' parameter.

    Returns:
    JsonResponse with status code and the list of countries
    and the specific artistic productions counter.
    """

    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return route_operations.error('Limite inválido.')

    if not limit > 0:
        return route_operations.error('Limite inválido.')

    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    result = []

    with open('util/countries.json', 'r') as f:
        country_dict = json.loads(f.read())

    countries_prod_art = query_set[0].producoes_artisticas \
        .all() \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]
    countries = {}

    for country in countries_prod_art:
        key = normalize_string(country['pais'])
        if key in countries:
            countries[key] += country['total']
        else:
            countries[key] = country['total']

    countries_prod_tec = query_set[0].producoes_tecnicas \
        .all() \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]

    for country in countries_prod_tec:
        key = normalize_string(country['pais'])
        if key in countries:
            countries[key] += country['total']
        else:
            countries[key] = country['total']

    countries_ban = query_set[0].bancas \
        .all() \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]

    for country in countries_ban:
        key = normalize_string(country['pais'])
        if key in countries:
            countries[key] += country['total']
        else:
            countries[key] = country['total']

    countries_part = query_set[0].participacoes \
        .all() \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]

    for country in countries_part:
        key = normalize_string(country['pais'])
        if key in countries:
            countries[key] += country['total']
        else:
            countries[key] = country['total']

    countries_ori = query_set[0].orientacao_set \
        .all() \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]

    for country in countries_ori:
        key = normalize_string(country['pais'])
        if key in countries:
            countries[key] += country['total']
        else:
            countries[key] = country['total']

    for key in countries:
        try:
            geolocation = country_dict[key]
            result.append({geolocation['name']: {'lat': geolocation['lat'],
                                                 'long': geolocation['long'],
                                                 'total': countries[key]}})
        except Exception:
            pass

    return JsonResponse({"countries": result})


def map_national(request):
    """National map for dashboard route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of states
    and the count of all productions.
   """
    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return route_operations.error('Limite inválido')

    if not limit > 0:
        return route_operations.error('Limite inválido')

    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    result = []

    prod_art_states_count = query_set[0].producoes_artisticas \
        .all() \
        .values('estado') \
        .annotate(total=Count('estado'))

    prod_bib_states_count = query_set[0].producoes_bibliograficas \
        .all() \
        .values('estado') \
        .annotate(total=Count('estado'))

    prod_tec_states_count = query_set[0].producoes_tecnicas \
        .all() \
        .values('estado') \
        .annotate(total=Count('estado'))

    states = {
        None: 0,
        "": 0,
        "Acre": 0,
        "Alagoas": 0,
        "Amapá": 0,
        "Amazonas": 0,
        "Bahia": 0,
        "Ceará": 0,
        "Distrito Federal": 0,
        "Espírito Santo": 0,
        "Goiás": 0,
        "Maranhão": 0,
        "Mato Grosso": 0,
        "Mato Grosso do Sul": 0,
        "Minas Gerais": 0,
        "Pará": 0,
        "Paraíba": 0,
        "Paraná": 0,
        "Pernambuco": 0,
        "Piauí": 0,
        "Rio de Janeiro": 0,
        "Rio Grande do Norte": 0,
        "Rio Grande do Sul": 0,
        "Rondônia": 0,
        "Roraima": 0,
        "Santa Catarina": 0,
        "São Paulo": 0,
        "Sergipe": 0,
        "Tocantins": 0
    }

    for state in prod_art_states_count:
        states[state["estado"]] += state["total"]
    for state in prod_bib_states_count:
        states[state["estado"]] += state["total"]
    for state in prod_tec_states_count:
        states[state["estado"]] += state["total"]

    if "" in states:
        del states[""]
    if None in states:
        del states[None]

    states_total = sum([total for total in states.values()])
    result = {"estados": states, "total": states_total}

    return JsonResponse(result)


def tiposProdArt(request):
    """Tipos for producao_artistica of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of artistic
    production's types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    types = query_set[0].producoes_artisticas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    result = []
    accepted = ['Outra_Producao_Artistica_Cultural', 'Artes_Visuais', 'Musica']
    for t in types:
        if t['tipo'] in accepted:
            result.append(t['tipo'])
    return JsonResponse({'production_types': result})


def countAll(request):
    """Count for pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {'DOCENTE': {year: 0 for year in labels}}

    prodsArt = query_set[0].producoes_artisticas.all()
    prodsArt = prodsArt.values('id', 'ano')

    prodsTec = query_set[0].producoes_tecnicas.all()
    prodsTec = prodsTec.values('id', 'ano')

    prodsBib = query_set[0].producoes_bibliograficas.all()
    prodsBib = prodsBib.values('id', 'ano')

    prodsBan = query_set[0].aparicoes_bancas.all()
    prodsBan = prodsBan.values('id', 'ano')

    prodsOri = query_set[0].orientacao_set.all()
    prodsOri = prodsOri.values('id', 'ano')

    prodsPre = query_set[0].premiostitulos_set.all()
    prodsPre = prodsPre.values('id', 'ano')

    prods = list(prodsArt.union(prodsTec, prodsBib, prodsBan, prodsOri,
                                prodsPre, all=True))

    for prod in prods:
        if (prod['ano'] >= ini_year and prod['ano'] <= end_year):
            result['DOCENTE'][prod['ano']] += 1

    return JsonResponse(result)


def countTiposProdArt(request):
    """Count_tipos for producao_artistica of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    artistic production's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    query = query_set[0].producoes_artisticas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    result = {}

    for t in requested_types:
        prods = query_set[0].producoes_artisticas \
            .all() \
            .filter(tipo=t) \
            .count()
        result[t] = prods

    return JsonResponse(result)


def tiposProdTec(request):
    """Tipos for producao_tecnica of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of technical
    production's types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    types = query_set[0].producoes_tecnicas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    result = []
    accepted = ['Trabalho_Tecnico', 'Organizacao_De_Evento',
                'Curso_De_Curta_Duracao_Ministrado', 'Programa_De_Radio_Ou_Tv',
                'Apresentacao_De_Trabalho']
    for t in types:
        if t['tipo'] in accepted:
            result.append(t['tipo'])
    return JsonResponse({'production_types': result})


def countTiposProdTec(request):
    """Count_tipos for producao_tecnica of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    technical production's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    query = query_set[0].producoes_tecnicas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    result = {}

    for t in requested_types:
        prods = query_set[0].producoes_tecnicas \
            .all() \
            .filter(tipo=t) \
            .count()
        result[t] = prods

    return JsonResponse(result)


def tiposProdBib(request):
    """Tipos for producao_bibliografica of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of bibliographic
    production's types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    types = query_set[0].producoes_bibliograficas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    result = []
    accepted = ['Livro', 'Capitulo', 'Artigo', 'Evento', 'Jornal',
                'Prefacio_Posfacio']
    for t in types:
        if t['tipo'] in accepted:
            result.append(t['tipo'])
    return JsonResponse({'production_types': result})


def countTiposProdBib(request):
    """Count_tipos for producao_bibliografica of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    bibliographic production's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    query = query_set[0].producoes_bibliograficas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    result = {}

    for t in requested_types:
        prods = query_set[0].producoes_bibliograficas \
            .all() \
            .filter(tipo=t) \
            .count()
        result[t] = prods

    return JsonResponse(result)


def tiposPart(request):
    """Tipos for participacao of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of participations
    types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    types = query_set[0].participacoes.all()\
        .values('tipo')\
        .distinct()\
        .order_by('tipo')
    result = []
    for t in types:
        result.append(t['tipo'])
    return JsonResponse(result)


def countTiposPart(request):
    """Count_tipos for participacao of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    participation's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    query = query_set[0].participacoes \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    result = {}

    for t in requested_types:
        prods = query_set[0].participacoes \
            .all() \
            .filter(tipo=t) \
            .count()
        result[t] = prods

    return JsonResponse(result)


def tiposBanca(request):
    """Tipos for bancas of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of bancas
    types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    types = query_set[0].aparicoes_bancas \
        .all() \
        .values('tipo_pesquisa') \
        .distinct() \
        .order_by('tipo_pesquisa')
    result = []
    accepted = ['Doutorado', 'Graduacao', 'Livre_Docencia', 'Mestrado',
                'Outra']
    for t in types:
        if t['tipo_pesquisa'] in accepted:
            result.append(t['tipo_pesquisa'])
    return JsonResponse({'production_types': result})


def countTiposBanca(request):
    """Count_tipos for bancas of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    banca's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    query = query_set[0].aparicoes_bancas \
        .all() \
        .values('tipo_pesquisa') \
        .distinct() \
        .order_by('tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    result = {}

    for t in requested_types:
        prods = query_set[0].aparicoes_bancas \
            .all() \
            .filter(tipo_pesquisa=t) \
            .count()
        result[t] = prods

    return JsonResponse(result)


def tiposOri(request):
    """Tipos for orientacoes of pessoa route.

    Parameters: a GET request with 'idLattes' parameter

    Returns:
    JsonResponse with status code and a list of orientacoes
    types of the given pessoa.
    """
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    types = query_set[0].orientacao_set \
        .all() \
        .values('tipo_pesquisa') \
        .distinct() \
        .order_by('tipo_pesquisa')
    result = []
    for t in types:
        result.append(t['tipo_pesquisa'])
    return JsonResponse({'production_types': result})


def countTiposOri(request):
    """Count_tipos for orientacao of pessoa route.

    Parameters: a GET request with 'tipos[]' and 'idLattes'

    Returns:
    JsonResponse with status code and a list with the amount of each
    orientacao's type for the given pessoa.
    """
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')
    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')
    query = query_set[0].orientacao_set \
        .all() \
        .values('tipo_pesquisa') \
        .distinct() \
        .order_by('tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    result = {}

    for t in requested_types:
        prods = query_set[0].orientacao_set \
            .all() \
            .filter(tipo_pesquisa=t) \
            .count()
        result[t] = prods

    return JsonResponse(result)


def countProdArtByYear(request):
    """Count for producao_artistica of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of artistic productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    query = query_set[0].producoes_artisticas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {
        type:
            {year: 0 for year in labels}
        for type in requested_types
    }

    prods = query.filter(tipo__in=requested_types)

    prods = prods \
        .values('ano', 'tipo') \
        .annotate(year_total=Count(['ano', 'tipo'])) \
        .order_by('tipo', 'ano')

    prods = list(prods)
    i = 0
    type = ""
    while i < len(prods):
        prod = prods[i]
        if prod['tipo'] != type:
            type = prod['tipo']
            while i < len(prods) and prod['tipo'] == type:
                prod = prods[i]
                result[type][prod['ano']] = prod['year_total']
                i += 1

    return JsonResponse(result)


def countProdTecByYear(request):
    """Count for producao_tecnica of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of technical productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    query = query_set[0].producoes_tecnicas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {
        type:
            {year: 0 for year in labels}
        for type in requested_types
    }

    prods = query.filter(tipo__in=requested_types)

    prods = prods \
        .values('ano', 'tipo') \
        .annotate(year_total=Count(['ano', 'tipo'])) \
        .order_by('tipo', 'ano')

    prods = list(prods)
    i = 0
    type = ""
    while i < len(prods):
        prod = prods[i]
        if prod['tipo'] != type:
            type = prod['tipo']
            while i < len(prods) and prod['tipo'] == type:
                prod = prods[i]
                result[type][prod['ano']] = prod['year_total']
                i += 1

    return JsonResponse(result)


def countProdBibByYear(request):
    """Count for producao_bibliografica of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of bibliographic productions
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    query = query_set[0].producoes_bibliograficas \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {
        type:
            {year: 0 for year in labels}
        for type in requested_types
    }

    prods = query.filter(tipo__in=requested_types)

    prods = prods \
        .values('ano', 'tipo') \
        .annotate(year_total=Count(['ano', 'tipo'])) \
        .order_by('tipo', 'ano')

    prods = list(prods)
    i = 0
    type = ""
    while i < len(prods):
        prod = prods[i]
        if prod['tipo'] != type:
            type = prod['tipo']
            while i < len(prods) and prod['tipo'] == type:
                prod = prods[i]
                result[type][prod['ano']] = prod['year_total']
                i += 1

    return JsonResponse(result)


def countPartByYear(request):
    """Count for participacao of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of participations
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    query = query_set[0].participacoes \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {
        type:
            {year: 0 for year in labels}
        for type in requested_types
    }

    prods = query.filter(tipo__in=requested_types)

    prods = prods \
        .values('ano', 'tipo') \
        .annotate(year_total=Count(['ano', 'tipo'])) \
        .order_by('tipo', 'ano')

    prods = list(prods)
    i = 0
    type = ""
    while i < len(prods):
        prod = prods[i]
        if prod['tipo'] != type:
            type = prod['tipo']
            while i < len(prods) and prod['tipo'] == type:
                prod = prods[i]
                result[type][prod['ano']] = prod['year_total']
                i += 1

    return JsonResponse(result)


def countBancaByYear(request):
    """Count for banca of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of bancas
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    query = query_set[0].aparicoes_bancas \
        .all() \
        .values('tipo_pesquisa') \
        .distinct() \
        .order_by('tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {
        type:
            {year: 0 for year in labels}
        for type in requested_types
    }

    prods = query.filter(tipo_pesquisa__in=requested_types)

    prods = prods \
        .values('ano', 'tipo_pesquisa') \
        .annotate(year_total=Count(['ano', 'tipo_pesquisa'])) \
        .order_by('tipo_pesquisa', 'ano')

    prods = list(prods)
    i = 0
    type = ""
    while i < len(prods):
        prod = prods[i]
        if prod['tipo_pesquisa'] != type:
            type = prod['tipo_pesquisa']
            while i < len(prods) and prod['tipo_pesquisa'] == type:
                prod = prods[i]
                result[type][prod['ano']] = prod['year_total']
                i += 1

    return JsonResponse(result)


def countOriByYear(request):
    """Count for orientacao of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'tipos[]' and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of orientacao
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    requested_types = request.GET.getlist('tipos[]')
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    query = query_set[0].orientacao_set \
        .all() \
        .values('tipo_pesquisa') \
        .distinct() \
        .order_by('tipo_pesquisa')
    valid_types = []
    for q in query:
        valid_types.append(q['tipo_pesquisa'])
    for t in requested_types:
        if t not in valid_types:
            return route_operations.error('Algum tipo é inválido.')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    result = {
        type:
            {year: 0 for year in labels}
        for type in requested_types
    }

    prods = query.filter(tipo_pesquisa__in=requested_types)

    prods = prods \
        .values('ano', 'tipo_pesquisa') \
        .annotate(year_total=Count(['ano', 'tipo_pesquisa'])) \
        .order_by('tipo_pesquisa', 'ano')

    prods = list(prods)
    i = 0
    type = ""
    while i < len(prods):
        prod = prods[i]
        if prod['tipo_pesquisa'] != type:
            type = prod['tipo_pesquisa']
            while i < len(prods) and prod['tipo_pesquisa'] == type:
                prod = prods[i]
                result[type][prod['ano']] = prod['year_total']
                i += 1

    return JsonResponse(result)


def countPremioByYear(request):
    """Count for premios of pessoa route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim'
    and 'idLattes' parameters.

    Returns:
    JsonResponse with status code and the count of premios
    for each year for the given person.
    """
    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    idLattes = request.GET.get('id', '')

    query_set = Pessoa.objects.filter(id_lattes=idLattes)
    if not query_set:
        return route_operations.error('Id inválido')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    query = query_set[0].premiostitulos_set.all()

    result = {
        'premios_titulos': {year: 0 for year in labels}
    }

    prods = query
    prods = prods \
        .values('ano') \
        .annotate(year_total=Count(['ano'])) \
        .order_by('ano')

    prods = list(prods)
    for prod in prods:
        result['premios_titulos'][prod['ano']] = prod['year_total']

    return JsonResponse(result)


def getPictureId(request):
    """Profile picture for pessoa route.

    Parameters: a GET request with 'idLattes' parameter.

    Returns:
    JsonResponse with status code and profile picture id.
    """

    id_lattes = request.GET.get('id', '')

    if not id_lattes.isdigit():
        return route_operations.error('Id inválido')

    user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) " \
        + "AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1" \
        + "Safari/605.1.15"
    headers = {"User-Agent": user_agent}
    lattes_response = requests.get(f'http://lattes.cnpq.br/{id_lattes}',
                                   headers=headers)
    picture_id = re.search(r'id" value="([^ ]*)"', lattes_response.text)
    return JsonResponse({"picture": picture_id.group(1)})


def aggregated_names(request):
    return JsonResponse({"names": services.get_aggregated_names()})
