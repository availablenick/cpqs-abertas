from models.models import Pessoa, Orientacao
import util.fau.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection

aggregates = db_connection.get_mongo_db().orientacao

def get_count(params):
    return route_operations.count(Orientacao, params)


def get_keywords(params):
    return route_operations.keywords(Orientacao, params)


def get_map(params):
    return route_operations.map(Orientacao, params)


def get_types_count(params):
    departments = params.getlist('departamentos[]')
    types = params.getlist('tipos[]')

    if not departments:
        departments = [dep['departamento']
                       for dep in Pessoa.objects.values('departamento')
                           .distinct()]
    if not types:
        types = [typ['tipo_pesquisa']
                 for typ in Orientacao.objects.values('tipo_pesquisa')
                     .distinct()]

    result = {}
    for department in departments:
        result[department] = []
        for type in types:
            values = {}
            ori_from_dep = Pessoa.objects \
                .filter(departamento__contains=department) \
                .values('id_lattes')
            ori_count = Orientacao.objects \
                .filter(pessoa__in=ori_from_dep) \
                .distinct() \
                .filter(tipo_pesquisa=type, pessoa__in=ori_from_dep) \
                .count()
            values[type] = ori_count
            result[department].append(values)

    return result


def get_types():
    result = [typ['tipo_pesquisa']
             for typ in Orientacao.objects.values('tipo_pesquisa')
                 .distinct()]
    return result


def get_aggregated_count():
    return aggregates.find_one({ "operation": "count" })["result"]


def get_aggregated_count_by_type():
    return aggregates.find_one({ "operation": "count_by_type" })["result"]


def get_aggregated_keywords():
    return aggregates.find_one({ "operation": "keywords" })["result"]


def get_aggregated_map():
    return aggregates.find_one({ "operation": "map" })["result"]


def get_aggregated_national_map():
    return aggregates.find_one({ "operation": "national_map" })["result"]


def get_aggregated_types_count():
    return aggregates.find_one({ "operation": "types_count" })["result"]
