import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import LineChart from "./LineChart";

const MAX_YEAR = new Date().getFullYear();

function LineChartContainer(props) {
  const {
    fetchData,
    formatData,
    labels,
    color,
    fill,
    initialStartYear,
    initialEndYear,
    Loading,
  } = props;
  const [lineChartData, setLineChartData] = useState(null);

  useEffect(() => {
    fetchData().then((data) => {
      setLineChartData(formatData(data));
    });
  }, []);

  return (
    <>
      {!lineChartData ? (
        <Loading />
      ) : (
        <LineChart
          color={color}
          fill={fill}
          initialEndYear={initialEndYear}
          initialStartYear={initialStartYear}
          labels={labels}
          countByYearByLabel={lineChartData}
        />
      )}
    </>
  );
}

LineChartContainer.propTypes = {
  color: PropTypes.object.isRequired,
  fill: PropTypes.object.isRequired,
  fetchData: PropTypes.func.isRequired,
  formatData: PropTypes.func,
  initialEndYear: PropTypes.number,
  initialStartYear: PropTypes.number,
  labels: PropTypes.array.isRequired,
  Loading: PropTypes.func,
};

LineChartContainer.defaultProps = {
  formatData: (data) => data,
  initialEndYear: MAX_YEAR - 1,
  initialStartYear: 1990,
  Loading: () => <></>,
};

export default LineChartContainer;
