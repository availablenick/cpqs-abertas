import styled from "styled-components";
import FEARPArrow from "./assets/images/FEARP-arrow.svg";
import RADArrow from "./assets/images/RAD-arrow.svg";
import RCCArrow from "./assets/images/RCC-arrow.svg";
import RECArrow from "./assets/images/REC-arrow.svg";

const highlightsFontFamily = "ITC Avant Garde Gothic Std";
const secondaryFontFamily = "DTL Prokyon";

const instituteColor = "#008DC8";
const supportColor = "#555555";

const instituteArrow = FEARPArrow;

const departmentArrows = {
  RAD: RADArrow,
  RCC: RCCArrow,
  REC: RECArrow,
};

const barColors = {
  RAD: ["#71050B", "#8D1111", "#B20610", "#E04C55", "#E67379", "#F4A2A8"],
  RCC: ["#895630", "#AC510D", "#D96D1D", "#F88E3E", "#FFB177", "#F5CAAD"],
  REC: ["#390E57", "#6508A3", "#9A42D5", "#AE62E1", "#C688F0", "#DBB1F8"],
  FEARP: ["#005072", "#0171A0", "#008DC8", "#54A5D6", "#9EC5E5", "#E5EFF9"],
  GRAYSCALE: ["#000000", "#4D4D4D", "#686868", "#808080", "#B3B3B3", "#E6E6E6"],
};

const departmentColors = {
  RAD: "#B20610",
  RCC: "#D96D1D",
  REC: "#6508A3",
  DOCENTE: "#000000",
};

const wordCloudColors = {
  RAD: ["#6d6d6d", "#585859", "#353535", "#000"],
  RCC: ["#6d6d6d", "#585859", "#353535", "#000"],
  REC: ["#6d6d6d", "#585859", "#353535", "#000"],
  default: ["#6d6d6d", "#585859", "#353535", "#000"],
};

const worldMapColors = {
  RAD: "#000000",
  RCC: "#000000",
  REC: "#000000",
};

const BarChartWrapper = styled.div`
  background-color: white;
  padding: 20px 0 20px 40px;
`;

const LineChartWrapper = styled.div`
  background-color: white;
  padding: 20px 0 20px 40px;

  .rc-slider-track {
    background-color: ${supportColor};
  }

  .rc-slider-handle {
    background-color: ${supportColor};
    border-color: ${supportColor};
  }

  .rc-slider-handle:hover {
    border-color: ${supportColor};
  }

  .rc-slider-handle:focus {
    border-color: ${supportColor};
  }

  .rc-slider-handle:active {
    border-color: ${supportColor};
    box-shadow: 0 0 5px ${supportColor};
  }

  .rc-slider-rail {
    background-color: #cccccc;
  }
`;

const NationalMapWrapper = styled.div`
  background-color: white;
`;

const WordCloudWrapper = styled.div`
  background-color: white;
  height: 100%;

  padding: 0 0 10px 10px;
`;

const WorldMapWrapper = styled.div`
  background-color: white;
`;

const CenteredColumn = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  grid-column-start: 3;
  grid-row-start: 2;
  overflow-y: auto;
  height: 100%;
  justify-content: center;
`;

const Body = styled.div`
  display: grid;
  font-family: ${secondaryFontFamily};
  grid-template-columns: 1fr 230px 1100px 1fr;
  grid-template-rows: 100px 590px 100px;
  height: 99.1vh;
  scrollbar-color: #fff #000;
  width: 100%;

  button,
  datalist,
  fieldset,
  input,
  label,
  legend,
  output,
  option,
  optgroup,
  select,
  textarea {
    font-family: ${secondaryFontFamily};
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
`;

const Conteudo = styled.div`
  grid-column-start: 3;
  grid-row-start: 2;
`;

const DivCard = styled.div`
  background-color: #fff;
  border-color: ${(props) => props.borderColor || supportColor};
  border-radius: 4px;
  border-style: solid;
  border-width: ${(props) => (props.borderColor ? "5px" : "2px")};
  display: grid;
  grid-template-columns 550px auto;
  grid-template-rows 40% 60%;
  height: 100%;
  position: relative;
  width: 100%;
`;

const DivInfo = styled.div`
  display: grid;
  grid-column-end: 2;
  grid-row-start: 1;
  grid-template-columns 193px 1fr;
  height: 100%;
`;

const DivInfoProducao = styled.div`
  grid-row-start: 1;
  grid-column-end: 2;
  height: 100%;
`;

const DivTotal = styled.div`
  display: flex;
  flex-flow: column;
  font-family: ${highlightsFontFamily};
  font-weight: bold;
  margin-top: 15px;
  width: 100%;

  > * {
    font-size: 25px;
  }

  > *:first-child {
    color: ${instituteColor};
    margin-bottom: -17px;
  }

  > *:last-child {
    color: ${supportColor};
  }
`;

const DivTitle = styled.div`
  color: ${supportColor};
  font-family: ${highlightsFontFamily};
  font-weight: bold;
  width: 90%;

  > span {
    font-size: 32px;
    font-weight: bold;
  }
`;

const DivGraph = styled.div`
  grid-row-start: 2;
  height: 100%;
  padding: 20px 20px 0 0;
  width: 100% .carousel.carousel-slider, .carousel-root {
    height: 100%;
  }

  .carousel .slide {
    background: #fff;
  }

  .carousel .control-arrow:before,
  .carousel.carousel-slider .control-arrow:before {
    border-top: 12px solid transparent;
    border-bottom: 12px solid transparent;
  }

  .carousel .control-next.control-arrow:before {
    border-left: 15px solid #000;
  }

  .carousel .control-prev.control-arrow:before {
    border-right: 15px solid #000;
  }

  .carousel .control-arrow,
  .carousel.carousel-slider .control-arrow {
    opacity: 1;
    margin-left: -10px;
    margin-right: -10px;
  }

  .carousel.carousel-slider .control-arrow:hover {
    background: rgba(0, 0, 0, 0);
  }
`;

const DivInfoText = styled.div`
  display: flex;
  flex-direction: column;
  grid-column-start: 2;
  grid-row-end: 3;
  grid-row-start: 1;
  overflow-x: hidden;
  padding: 20px;

  > .title {
    font-family: ${highlightsFontFamily};
  }

  > span {
    :nth-child(2) {
      margin-top: -10px;
    }

    font-size: 13.5px;
    line-height: 1.5;
    margin-bottom: 10px;
  }
`;

const DivInfoProf = styled.div`
  display: flex;
  flex-flow: column;
  padding-top: 20px;
  width: 90%;

  a {
    color: ${(props) => (props.color ? props.color : "#000")};
    font-size: 15px;
    font-weight: bold;
    text-decoration: none;
  }

  > *:first-child {
    font-family: ${highlightsFontFamily};
  }

  > span:nth-child(odd) {
    font-size: 16px;
    font-weight: bold;
  }

  > span:nth-child(even) {
    color: #000;
    font-size: 15px;
    margin-bottom: 3px;
    margin-top: 13px;
  }
`;

const DivLink = styled.div`
  > div {
    display: flex;
    flex-direction: column;
    flex-overflow: column;

    > span#ano {
      font-size: 25px;
      font-weight: bold;
    }

    div > span {
      margin-bottom: 5px;
    }
  }
`;

const DivLinkDocentes = styled.div`
  align-items: flex-start;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 100%;
  > span {
    font-size: 15px;
    font-weight: bold;
    margin-right: 20px;
    opacity: 1;

    :not(:last-child) {
      margin-bottom: 3.7px;
    }

    a {
      color: #000;
      font-size: 14px;
      font-weight: bold;
      margin-bottom: 3.7px;
      opacity: 0.8;
      text-decoration: none;
    }
  }
`;

const DivDetailBlock = styled.div`
  a {
    color: ${(props) => (props.color ? props.color : "#000")};
    font-size: 15px;
    font-weight: bold;
    text-decoration: none;
  }
`;

export {
  highlightsFontFamily,
  barColors,
  departmentColors,
  wordCloudColors,
  worldMapColors,
  instituteArrow,
  departmentArrows,
  instituteColor,
  supportColor,
  BarChartWrapper,
  Body,
  CenteredColumn,
  Conteudo,
  DivCard,
  DivDetailBlock,
  DivGraph,
  DivInfo,
  DivInfoProducao,
  DivInfoProf,
  DivInfoText,
  DivLink,
  DivLinkDocentes,
  DivTitle,
  DivTotal,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
};
