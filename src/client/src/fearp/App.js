import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/Home";
import HeaderWrapper from "./components/wrappers/HeaderWrapper";
import Menu from "./components/Menu";
import FooterWrapper from "./components/wrappers/FooterWrapper";
import Docentes from "./pages/Docentes";
import RAD from "./pages/RAD";
import RCC from "./pages/RCC";
import REC from "./pages/REC";
import PerfilDocente from "./pages/PerfilDocente";
import ProducaoBibliografica from "./pages/ProducaoBibliografica";
import ProducaoArtistica from "./pages/ProducaoArtistica";
import ProducaoTecnica from "./pages/ProducaoTecnica";
import Orientacoes from "./pages/Orientacoes";
import Bancas from "./pages/Bancas";
import PremiosETitulos from "./pages/PremiosETitulos";
import PerfilProdBiblio from "./pages/PerfilProdBiblio";
import PerfilProdArt from "./pages/PerfilProdArt";
import PerfilProdTec from "./pages/PerfilProdTec";
import PerfilOrientacoes from "./pages/PerfilOrientacoes";
import PerfilBancas from "./pages/PerfilBancas";
import PerfilPremios from "./pages/PerfilPremios";
import FAQ from "./components/faq/FAQ";
import { Body } from "./styles";
import "./fonts.scss";

function App() {
  const [menuFilter, setMenuFilter] = useState({
    home: true,
    docentes: false,
    RAD: false,
    RCC: false,
    REC: false,
    perfil: false,
    bibliografica: false,
    artistica: false,
    tecnica: false,
    orientacoes: false,
    bancas: false,
    premios: false,
    faq: false,
  });

  useEffect(() => {
    document.querySelector("title").innerText = "FEA-RP Aberta";
    document.querySelector('link[rel="shortcut icon"]').href =
      "/fearp/favicon.ico";
  }, []);

  return (
    <Router>
      <Body>
        <HeaderWrapper />
        <Menu menuFilter={menuFilter} setMenuFilter={setMenuFilter} />

        {menuFilter.faq && <FAQ setMenuFilter={setMenuFilter} />}

        <Switch>
          <Route exact path="/">
            <Home setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/RAD">
            <RAD setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/REC">
            <REC setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/RCC">
            <RCC setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/producao-bibliografica">
            <ProducaoBibliografica setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/producao-artistica">
            <ProducaoArtistica setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/producao-tecnica">
            <ProducaoTecnica setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/orientacoes">
            <Orientacoes setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/bancas">
            <Bancas setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/premios-e-titulos">
            <PremiosETitulos setMenuFilter={setMenuFilter} />
          </Route>

          <Route exact path="/pessoa/nomes">
            <Docentes setMenuFilter={setMenuFilter} department="all" />
          </Route>
          <Route path="/pessoa/nomes/RAD">
            <Docentes setMenuFilter={setMenuFilter} department="RAD" />
          </Route>
          <Route path="/pessoa/nomes/REC">
            <Docentes setMenuFilter={setMenuFilter} department="REC" />
          </Route>
          <Route path="/pessoa/nomes/RCC">
            <Docentes setMenuFilter={setMenuFilter} department="RCC" />
          </Route>
          <Route path="/pessoa/perfil/:id">
            <PerfilDocente
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/producao-bibliografica/:id">
            <PerfilProdBiblio
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/producao-artistica/:id">
            <PerfilProdArt
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/producao-tecnica/:id">
            <PerfilProdTec
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/bancas/:id">
            <PerfilBancas
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/orientacoes/:id">
            <PerfilOrientacoes
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/premios-e-titulos/:id">
            <PerfilPremios
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
        </Switch>

        <FooterWrapper />
      </Body>
    </Router>
  );
}

export default App;
