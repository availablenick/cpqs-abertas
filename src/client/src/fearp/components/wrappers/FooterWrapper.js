import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { getLastUpdateDate } from "../../../common/API";
import Footer from "../../../common/misc/Footer";
import { instituteColor } from "../../styles";

function FooterWrapper() {
  const [lastDataUpdateDate, setLastDataUpdateDate] = useState(null);

  useEffect(() => {
    getLastUpdateDate().then((res) => {
      setLastDataUpdateDate(res);
    });
  }, []);

  return (
    <Footer
      font="ITC Avant Garde Gothic Std"
      lastDataUpdateDate={lastDataUpdateDate}
      lastDataUpdateNoticeColor={instituteColor}
    >
      <Info>
        <div>
          <b style={{ color: instituteColor }}>
            Faculdade de Economia e Administração e Contabilidade de Ribeirão
            Preto
          </b>
        </div>
        <div>
          Avenida Bandeirantes, 3900 - Monte Alegre, Ribeirão Preto - SP,
          14040-905
        </div>
        <div>(16) 3315-3000</div>
      </Info>
      <Info>
        <div>
          <b style={{ color: instituteColor }}>
            Comissão de Pesquisa - CPq FEA-RP/USP
          </b>
        </div>
        <div>
          contato:{" "}
          <a
            style={{ textDecoration: "none", color: "#000" }}
            href="mailto:cpqs-abertas@usp.br"
          >
            cpqs-abertas@usp.br
          </a>
        </div>
        <div>(16) 3602-4961</div>
      </Info>
    </Footer>
  );
}

const Info = styled.div`
  align-items: center;
  color: black;
  display: flex;
  flex-direction: column;

  > div {
    font-size: 10px;
    margin-bottom: 4px;
    text-align: center;
  }

  > a {
    color: white;
    font-size: 20px;
    text-decoration: none;
  }
`;

export default FooterWrapper;
