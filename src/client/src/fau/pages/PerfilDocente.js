import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  Conteudo,
  DivCard,
  DivGraph,
  DivInfoText,
  departmentColors,
  wordCloudColors,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../styles";
import {
  getPessoa,
  getKeywordsPessoa,
  getMapPessoa,
  getMapPessoaNational,
  getCountAllPessoa,
  getDashboardCountByDep,
} from "../../common/API";
import ProfessorLattesInfo from "../components/profile/ProfessorLattesInfo";
import LineChartContainer from "../../common/charts/LineChartContainer";
import NationalMapContainer from "../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../common/maps/WorldMapContainer";
import SpinnerWrapper from "../components/wrappers/SpinnerWrapper";
import { setFilters } from "../helpers";

function PerfilDocente(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [data, setData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setData(responsePayload);

      const dep = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          [dep]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!data) {
    return (
      <Conteudo
        style={{
          alignItems: "center",
          border: "0",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <SpinnerWrapper />
      </Conteudo>
    );
  }

  return <Perfil data={data} id={id} menuFilter={menuFilter} />;
}

function Perfil(props) {
  const { data, id, menuFilter } = props;

  if (!data.departamento) {
    return (
      <Conteudo style={{ justifyContent: "start", width: 1100 }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  return (
    <Conteudo style={{ justifyContent: "start", width: 1100 }}>
      <DivCard>
        <ProfessorLattesInfo
          lattesId={data.id_lattes}
          contactNumber={data.contato}
          fullName={data.nome_completo}
          menuFilter={menuFilter}
          departament={data.departamento}
        />

        <DivGraph style={{ paddingLeft: "20px" }}>
          <Carousel
            showArrows
            showThumbs={false}
            showIndicators={false}
            showStatus={false}
            autoPlay
            infiniteLoop
            interval={5000}
          >
            <WordCloudWrapper>
              <WordCloudContainer
                colors={wordCloudColors[data.departamento]}
                fetchData={() => getKeywordsPessoa({ id, limit: 50 })}
                Loading={SpinnerWrapper}
              />
            </WordCloudWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                fetchData={() => getMapPessoa({ id })}
                Loading={SpinnerWrapper}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                fetchData={() => getMapPessoaNational({ id })}
                Loading={SpinnerWrapper}
              />
            </NationalMapWrapper>
            <LineChartWrapper>
              <LineChartContainer
                labels={["DOCENTE", data.departamento]}
                color={{
                  DOCENTE: departmentColors.DOCENTE,
                  [data.departamento]: departmentColors[data.departamento],
                }}
                fill={{
                  DOCENTE: "origin",
                  [data.departamento]: "-1",
                }}
                fetchData={async () => {
                  const professor = await getPessoa({ id });
                  const dep = professor.departamento;
                  const countByYearByDep = await getDashboardCountByDep({
                    ano_inicio: 1978,
                    departamento: dep,
                  });

                  const professorCountByYear = await getCountAllPessoa({ id });
                  return {
                    DOCENTE: professorCountByYear.DOCENTE,
                    [dep]: countByYearByDep[dep],
                  };
                }}
                Loading={SpinnerWrapper}
              />
            </LineChartWrapper>
          </Carousel>
        </DivGraph>

        <DivInfoText>
          <span
            style={{
              fontSize: 25,
              fontWeight: "bold",
              marginTop: -3.8,
            }}
          >
            Resumo Acadêmico
          </span>
          <span>{data.resumo}</span>
        </DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

PerfilDocente.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

Perfil.propTypes = {
  data: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  menuFilter: PropTypes.object.isRequired,
};

export default PerfilDocente;
