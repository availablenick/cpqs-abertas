import React from "react";
import Logo from "../../assets/images/ime.png";
import Fader from "../../../common/misc/Fader";

function FaderWrapper() {
  return <Fader image={Logo} width={120} height={45} />;
}

export default FaderWrapper;
