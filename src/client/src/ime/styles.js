import styled from "styled-components";
import IMEArrow from "./assets/images/Seta Carrossel Principal.svg";
import DCCArrow from "./assets/images/Seta Carrossel DCC.svg";
import MAEArrow from "./assets/images/Seta Carrossel MAE.svg";
import MAPArrow from "./assets/images/Seta Carrossel MAP.svg";
import MATArrow from "./assets/images/Seta Carrossel MAT.svg";

const fontFamily = "arial";

const instituteColor = "#9D0A0E";
const supportColor = "#555555";

const instituteArrow = IMEArrow;

const departmentArrows = {
  DCC: DCCArrow,
  MAE: MAEArrow,
  MAP: MAPArrow,
  MAT: MATArrow,
};

const barColors = {
  DCC: ["#650C0E", "#9D0A0E", "#C4393C", "#DB6E70", "#FEA7A9", "#F8C2C4"],
  MAE: ["#650C0E", "#9D0A0E", "#C4393C", "#DB6E70", "#FEA7A9", "#F8C2C4"],
  MAT: ["#650C0E", "#9D0A0E", "#C4393C", "#DB6E70", "#FEA7A9", "#F8C2C4"],
  MAP: ["#650C0E", "#9D0A0E", "#C4393C", "#DB6E70", "#FEA7A9", "#F8C2C4"],
  IME: ["#650C0E", "#9D0A0E", "#C4393C", "#DB6E70", "#FEA7A9", "#F8C2C4"],
  DOCENTE: ["#000", "#4d4d4d", "#686868", "#808080", "#b3b3b3", "#e6e6e6"],
};

const departmentColors = {
  DCC: "#650C0E",
  MAE: "#C4393C",
  MAT: "#DB6E70",
  MAP: "#FEA7A9",
  IME: "#9D0A0E",
  DOCENTE: "#555555",
};

const wordCloudColors = {
  DCC: ["#F8C2C4", "#DB6E70", "#9D0A0E", "#650C0E"],
  MAE: ["#F8C2C4", "#DB6E70", "#9D0A0E", "#650C0E"],
  MAT: ["#F8C2C4", "#DB6E70", "#9D0A0E", "#650C0E"],
  MAP: ["#F8C2C4", "#DB6E70", "#9D0A0E", "#650C0E"],
  default: ["#F8C2C4", "#DB6E70", "#9D0A0E", "#650C0E"],
};

const worldMapColors = {
  DCC: "#000000",
  MAE: "#000000",
  MAT: "#000000",
  MAP: "#000000",
};

const BarChartWrapper = styled.div`
  background-color: white;
  padding: 20px 0 20px 40px;
`;

const LineChartWrapper = styled.div`
  background-color: white;
  padding: 20px 0 20px 40px;

  .rc-slider-track {
    background-color: ${supportColor};
  }

  .rc-slider-handle {
    background-color: ${supportColor};
    border-color: ${supportColor};
  }

  .rc-slider-handle:hover {
    border-color: ${supportColor};
  }

  .rc-slider-handle:focus {
    border-color: ${supportColor};
  }

  .rc-slider-handle:active {
    border-color: ${supportColor};
    box-shadow: 0 0 5px ${supportColor};
  }

  .rc-slider-rail {
    background-color: #cccccc;
  }
`;

const NationalMapWrapper = styled.div`
  background-color: white;
`;

const WordCloudWrapper = styled.div`
  background-color: white;
  height: 100%;
  padding-bottom: 10px;
`;

const WorldMapWrapper = styled.div`
  background-color: white;
`;

const CenteredColumn = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  grid-column-start: 3;
  grid-row-start: 2;
  overflow-y: auto;
  height: 100%;
  justify-content: center;
`;

const Body = styled.div`
  display: grid;
  font-family: ${fontFamily};
  grid-template-columns: 1fr 230px 1100px 1fr;
  grid-template-rows: 100px 590px 100px;
  height: 99.1vh;
  scrollbar-color: #fff #000;
  width: 100%;

  button,
  datalist,
  fieldset,
  input,
  label,
  legend,
  output,
  option,
  optgroup,
  select,
  textarea {
    font-family: ${fontFamily};
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
`;

const Conteudo = styled.div`
  grid-column-start: 3;
  grid-row-start: 2;
`;

const DivCard = styled.div`
  background-color: #fff;
  border-color: ${(props) => props.borderColor || supportColor};
  border-style: solid;
  border-width: 5px;
  display: grid;
  grid-template-columns 550px auto;
  grid-template-rows 40% 60%;
  height: 100%;
  position: relative;
  width: 100%;
`;

const DivInfo = styled.div`
  display: grid;
  grid-column-end: 2;
  grid-row-start: 1;
  grid-template-columns 193px 1fr;
  height: 100%;
`;

const DivInfoProducao = styled.div`
  display: grid;
  grid-row-start: 1;
  grid-column-end: 2;
  height: 100%;
`;

const DivTotal = styled.div`
  display: flex;
  flex-flow: column;
  grid-row-start: 2;
  width: 100%;

  > span {
    font-size: 25px;
    font-weight: bold;
  }

  > *:first-child {
    color: ${instituteColor};
    margin-bottom: -17px;
  }

  > *:last-child {
    color: ${supportColor};
  }
`;

const DivTitle = styled.div`
  display: flex;
  flex-flow: column;
  color: ${supportColor};
  width: 90%;

  > span {
    font-size: 25px;
    font-weight: bold;
  }
`;

const DivGraph = styled.div`
  grid-row-start: 2;
  height: 100%;
  padding: 20px 20px 0 0;
  width: 100% .carousel.carousel-slider, .carousel-root {
    height: 100%;
  }

  .carousel .slide {
    background: #fff;
  }

  .carousel .control-arrow:before,
  .carousel.carousel-slider .control-arrow:before {
    border-top: 12px solid transparent;
    border-bottom: 12px solid transparent;
  }

  .carousel .control-next.control-arrow:before {
    border-left: 15px solid #000;
  }

  .carousel .control-prev.control-arrow:before {
    border-right: 15px solid #000;
  }

  .carousel .control-arrow,
  .carousel.carousel-slider .control-arrow {
    opacity: 1;
    margin-left: -10px;
    margin-right: -10px;
  }

  .carousel.carousel-slider .control-arrow:hover {
    background: rgba(0, 0, 0, 0);
  }
`;

const DivInfoText = styled.div`
  display: flex;
  flex-direction: column;
  grid-column-start: 2;
  grid-row-end: 3;
  grid-row-start: 1;
  overflow-x: hidden;
  padding: 20px;
  color: ${supportColor};

  > span {
    :nth-child(2) {
      margin-top: -10px;
    }

    font-size: 13.5px;
    line-height: 1.5;
    margin-bottom: 10px;
  }
`;

const DivDep = styled.div`
  color: #ffffff;
  align-items: center;
  background-position: center;
  background-position-y: -7.5px;
  background-repeat: no-repeat;
  background-size: 138px MATo;
  border-radius: 50%;
  display: flex;
  font-size: 26px;
  font-weight: bold;
  height: 150px;
  justify-content: center;
  margin: 20px;
  width: 150px;
`;

const DivInfoProf = styled.div`
  display: flex;
  flex-flow: column;
  padding-top: 20px;
  width: 90%;

  a {
    color: ${(props) => (props.color ? props.color : "#000")};
    font-size: 15px;
    font-weight: bold;
    text-decoration: none;
  }

  > span:nth-child(odd) {
    color: ${supportColor};
    font-size: 16px;
    font-weight: bold;
  }

  > span:nth-child(even) {
    color: #000;
    font-size: 15px;
    margin-bottom: 3px;
    margin-top: 13px;
  }
`;

const DivLink = styled.div`
  > div {
    display: flex;
    flex-direction: column;
    flex-overflow: column;

    > span#ano {
      font-size: 25px;
      font-weight: bold;
    }

    div > span {
      margin-bottom: 5px;
    }
  }
`;

const DivLinkDocentes = styled.div`
  align-items: flex-start;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  flex-wrap:wrap;
  width: 100%;

  > span {
    font-size: 15px;
    font-weight: bold;
    margin-right:20px;
    opacity: 1

    :not(:last-child) {
      margin-bottom: 3.7px;
    }

    a {
      color: #000;
      font-size: 14px;
      font-weight: bold;
      margin-bottom: 3.7px;
      opacity: 0.8;
      text-decoration: none;
    }
  }
`;

const DivDetailBlock = styled.div`
  a {
    color: ${(props) => (props.color ? props.color : "#000")};
    font-size: 15px;
    font-weight: bold;
    text-decoration: none;
  }
`;

export {
  barColors,
  departmentColors,
  wordCloudColors,
  worldMapColors,
  instituteColor,
  supportColor,
  instituteArrow,
  departmentArrows,
  BarChartWrapper,
  Body,
  CenteredColumn,
  Conteudo,
  DivCard,
  DivDep,
  DivDetailBlock,
  DivGraph,
  DivInfo,
  DivInfoProducao,
  DivInfoProf,
  DivInfoText,
  DivLink,
  DivLinkDocentes,
  DivTitle,
  DivTotal,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
};
