import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function MAP(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="MAP"
      depDescription="Departamento de Matemática Aplicada"
      chefe="Prof. Dr. Fábio Armando Tal"
      viceChefe="Prof. Dr. André Salles de Oliveira"
    >
      <span style={{ fontSize: 25, fontWeight: "bold" }}>Histórico</span>
      <span>
        O Departamento de Matemática Aplicada do IME-USP (MAP) foi constituido
        em 1971. O MAP é responsável pelo Bacharelado em Matemática Aplicada
        (BMA) e Bacharelado em Matemática Aplicada e Computacional (BMAC), além
        de oferecer disciplinas para diversas unidades da USP (a disciplina de
        Cálculo Numérico é oferecida para praticamente todos os alunos de
        Ciências Exatas do campus da Capital). Na pós-graduação o departamento
        conta com um programa de mestrado e doutorado, ambos constantemente bem
        avaliados pela CAPES, no qual se formaram várias dezenas de mestres e
        doutores.
      </span>
    </Department>
  );
}

MAP.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default MAP;
