import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function MAE(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="MAE"
      depDescription="Departamento de Estatística"
      chefe="Prof. Dr. Fábio Prattes Machado"
      viceChefe="Profa. Dra. Florencia Graciela Leonardi"
    >
      <span style={{ fontSize: 25, fontWeight: "bold" }}>Histórico</span>
      <span>
        O Departamento de Estatística, criado junto com o IME em 1970, reúne
        pesquisadores das áreas de Probabilidade e Estatística, e é responsável
        por disciplinas e cursos em níveis de graduação, aperfeiçoamento e pós
        graduação. Oferece o Bacharelado em Estatística, em nível de graduação,
        o Aperfeiçoamento em Tópicos de Estatística, em nível de
        aperfeiçoamento, e o Mestrado em Estatística e o Doutorado em
        Estatística, em nível de pós graduação.
      </span>
      <span>
        O Departamento de Estatística também contribui com disciplinas e
        serviços para outros programas do IME e da USP: disciplinas básicas de
        Probabilidade e Estatística, oferecidas a toda a USP, e outras, mais
        avançadas, para cursos específicos, e serviço de assessoria a
        pesquisadores da USP e outras instituições. O Departamento de
        Estatística também contribui com disciplinas e serviços para outros
        programas do IME e da USP: disciplinas básicas de Probabilidade e
        Estatística, oferecidas a toda a USP, e outras, mais avançadas, para
        cursos específicos, e serviço de assessoria a pesquisadores da USP e
        outras instituições.
      </span>
    </Department>
  );
}

MAE.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default MAE;
