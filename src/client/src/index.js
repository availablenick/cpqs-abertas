import React, { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';

const App = lazy(() => {
	return import(`./${process.env.REACT_APP_INSTITUTE}/App`);
});

ReactDOM.render(
	<Suspense fallback={<div></div>}>
		<App />
	</Suspense>,
	document.getElementById('root')
);
